mod utils;

extern crate fixedbitset;
extern crate js_sys;
extern crate web_sys;

use wasm_bindgen::prelude::*;
use std::fmt;
use fixedbitset::FixedBitSet;

// A macro to provide `println!(..)`-style syntax for `console.log` logging.
macro_rules! log {
  ( $( $t:tt )* ) => {
    web_sys::console::log_1(&format!( $( $t )* ).into());
  }
}

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Universe {
  width: u32,
  height: u32,
  cells: FixedBitSet,
}

#[wasm_bindgen]
impl Universe {
  pub fn new() -> Universe {
    utils::set_panic_hook();

    let width = 128;
    let height = 128;

    let size = (width * height) as usize;
    let mut cells = FixedBitSet::with_capacity(size);

    for i in 0..size {
      cells.set(i, js_sys::Math::random() < 0.5);
    }

    Universe {
      width,
      height,
      cells,
    }
  }

  pub fn set_width(&mut self, width: u32) {
    let size = (self.width * self.height) as usize;
    for i in 0..size {
      self.cells.set(i, false);
    }
    self.width = width;
  }

  pub fn width(&self) -> u32 {
    self.width
  }

  pub fn set_height(&mut self, height: u32) {
    let size = (self.width * self.height) as usize;
    for i in 0..size {
      self.cells.set(i, false);
    }
    self.height = height;
  }

  pub fn height(&self) -> u32 {
    self.height
  }

  pub fn cells(&self) -> *const u32 {
    self.cells.as_slice().as_ptr()
  }

  pub fn toggle_cell(&mut self, row: u32, column: u32) {
    let idx = self.get_index(row, column);
    self.cells.toggle(idx);
  }

  pub fn tick(&mut self) {
    let mut next = self.cells.clone();

    for row in 0..self.height {
      for col in 0..self.width {
        let idx = self.get_index(row, col);
        let cell = self.cells[idx];
        let live_neighbours = self.live_neighbour_count(row, col);

        next.set(idx, match (cell, live_neighbours) {
          // Rule 1: Any live cell with fewer than two live neighbours dies
          (true, x) if x < 2 => false,
          // Rule 2: Any live cell with two or three live neighbours carries on
          (true, 2) | (true, 3) => true,
          // Rule 3: Any live cell with more than three live neighbours dies
          (true, x) if x > 3 => false,
          // Rule 4: Any dead cell with exactly three live neighbours resurrects
          (false, 3) => true,
          // Any other case remains the same
          (otherwise, _) => otherwise
        });
      }
    }

    self.cells = next;
  }

  fn get_index(&self, row: u32, column: u32) -> usize {
    (row * self.width + column) as usize
  }

  fn live_neighbour_count(&self, row: u32, column: u32) -> u8 {
    let mut count = 0;

    let north = if row == 0 {
        self.height - 1
    } else {
        row - 1
    };

    let south = if row == self.height - 1 {
        0
    } else {
        row + 1
    };

    let west = if column == 0 {
        self.width - 1
    } else {
        column - 1
    };

    let east = if column == self.width - 1 {
        0
    } else {
        column + 1
    };

    let nw = self.get_index(north, west);
    count += self.cells[nw] as u8;

    let n = self.get_index(north, column);
    count += self.cells[n] as u8;

    let ne = self.get_index(north, east);
    count += self.cells[ne] as u8;

    let w = self.get_index(row, west);
    count += self.cells[w] as u8;

    let e = self.get_index(row, east);
    count += self.cells[e] as u8;

    let sw = self.get_index(south, west);
    count += self.cells[sw] as u8;

    let s = self.get_index(south, column);
    count += self.cells[s] as u8;

    let se = self.get_index(south, east);
    count += self.cells[se] as u8;

    count
  }
}

// Create a new implementation to not expose these functions to JS
impl Universe {
  pub fn get_cells(&self) -> &FixedBitSet {
    &self.cells
  }

  pub fn set_cells(&mut self, cells: &[(u32, u32)]) {
    for (row, col) in cells.iter().cloned() {
      let idx = self.get_index(row, col);
      self.cells.set(idx, true);
    }
  }
}

